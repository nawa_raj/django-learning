from django.urls import path
from rest_framework import routers

from . import views

app_name = "core_api"
router = routers.SimpleRouter()

# for category routes...
router.register(r'categories', views.CategoryModelViewSet, basename='category_routes')
router.register(r'transactions', views.TransactionModelViewSet, basename='transaction_routes')

urlpatterns = [
                  path("currencies/", views.CurrencyListAPIView.as_view(), name="currencies")
              ] + router.urls
