from django.contrib import admin

from . import models


# Register your models here.

@admin.register(models.Currency)
class AdminCurrency(admin.ModelAdmin):
    list_display = ('id', 'code', 'name')


# category table
@admin.register(models.Category)
class AdminCategory(admin.ModelAdmin):
    list_display = ('id', 'name')


# category table
@admin.register(models.Transaction)
class AdminTransaction(admin.ModelAdmin):
    list_display = ('id', 'currency', 'amount', 'date', 'category', 'description')
