from typing import Any
from rest_framework import status
from rest_framework.response import Response


class APIResponce:
    successMessage = "success".upper()
    failedMessage = "failed".upper()
    infoMessage = "Infomed".upper()
    redirectMessage = "Redirected".upper()
    serverErrorMessage = "ServerError".upper()

    def __init__(
        self,
        data: Any,
        status: int,
        message: str,
        template_name=None,
        headers=None,
        exception=False,
        content_type=None,
    ):
        self.status_code = status
        self.message = message
        self.data = data
        self.template_name = template_name
        self.headers = headers
        self.exception = exception
        self.content_type = content_type

    def getStatus(self):
        if status.is_success(self.status_code):
            return self.successMessage

        elif status.is_client_error(self.status_code):
            return self.failedMessage

        elif status.is_informational(self.status_code):
            return self.infoMessage

        elif status.is_redirect(self.status_code):
            return self.redirectMessage

        else:
            return self.serverErrorMessage

    def conventionFormat(self) -> dict[str, Any]:
        return {
            "status": self.getStatus(),
            "status_code": self.status_code,
            "message": self.message,
            "data": self.data,
        }

    def getConventionResponse(self):
        return Response(
            data=self.conventionFormat(),
            status=self.status_code,
            template_name=self.template_name,
            headers=self.headers,
            exception=self.exception,
            content_type=self.content_type,
        )

    def getDefaultResponse(self):
        return Response(
            data=self.data,
            status=self.status_code,
            template_name=self.template_name,
            headers=self.headers,
            exception=self.exception,
            content_type=self.content_type,
        )


class DemoTest(APIResponce):
    def __init__(
        self,
        status: int,
        message: str,
        data: Any,
    ):
        super().__init__(status, message, data)
        self.testValue = "testValue"
