from rest_framework import status
from rest_framework.views import APIView

from .models import Student
from .serializers import StudentSerializer
from utilities.apiResponse import APIResponce


class StudentAPI(APIView):
    def get(self, request, pk=None, format=None):

        if pk is not None:
            try:
                studentObj = Student.objects.get(pk=pk)
                serializedData = StudentSerializer(studentObj).data
                response = response = APIResponce(
                    data=serializedData,
                    status=status.HTTP_200_OK,
                    message="Successfully Get Requested data".title(),
                ).getConventionResponse()
                return response

            except Student.DoesNotExist:
                response = APIResponce(
                    data=None,
                    status=status.HTTP_400_BAD_REQUEST,
                    message="Requested data not found".title(),
                ).getConventionResponse()
                return response

        studentList = Student.objects.all()
        serializedData = StudentSerializer(studentList, many=True).data
        response = APIResponce(
            data=serializedData,
            status=status.HTTP_200_OK,
            message="Successfully Get Requested data".title(),
        ).getConventionResponse()
        return response
